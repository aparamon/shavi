# Shavi: SHACL Visualization tool

### Installation

Pre-requisites:
[Python 3](https://www.python.org/downloads), [RDFLib](https://pypi.org/project/rdflib), [GraphViz](https://www.graphviz.org/download).

Drop Allotrope ontology TTL files in **ontologies** dir.

### Usage

`> shavi shacl.ttl`

(if you are on Windows, make sure GraphViz executables are in PATH)

### TODO
* Support CMap output
* Cache dependent ontologies for faster loading
* Investigate download errors
* More layout engines
* Dot-file output (for Gephi etc)
* Node coloring
