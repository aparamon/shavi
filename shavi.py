import sys
import os, os.path
import rdflib, rdflib.exceptions, rdflib.plugins.parsers.pyRdfa, rdflib.util
import urllib, urllib.error
from functools import *
from itertools import *
import subprocess
import string
import math
import re

def getfiles(dir):
    for f in os.listdir(dir):
        if os.path.isfile(os.path.join(dir, f)):
            yield os.path.join(dir, f)

scriptdir = os.path.dirname(os.path.abspath(__file__))

# load model
model = rdflib.Graph()
fmt = rdflib.util.guess_format(sys.argv[1])
print('Input format is {}.'.format(fmt), file = sys.stderr)
model.parse(sys.argv[1], format = fmt)

# load dependencies
deps = rdflib.Graph()
ontology = rdflib.graph.ReadOnlyGraphAggregate([model, deps])

localOntologies = dict()
nsre = re.compile('\@prefix\s+\:\s+\<(.+)\>\s+\.')
for ttlfile in getfiles(os.path.join(scriptdir, 'ontologies')):
    namespace = nsre.search(open(ttlfile, encoding = 'utf8').readline()).group(1).strip('#')
    localOntologies[namespace] = ttlfile
for prefix, namespace in model.namespaces():
    namespace = namespace.strip('#')
    print('Loading {} ...'.format(namespace), file = sys.stderr)
    try:
        if namespace in localOntologies:
            deps.parse(localOntologies[namespace], format = 'turtle')
        else:
            deps.parse(namespace)
    except (rdflib.exceptions.Error, rdflib.plugins.parsers.pyRdfa.RDFaError, urllib.error.URLError) as e:
        print(e, file = sys.stderr)

# make dot-file
def isdefined(model, node):
    for obj in model.objects(subject = node):
        return True

prefixre = re.compile('^.*?:')

@lru_cache()
def getlabel(node):
    if isinstance(node, rdflib.BNode):
        return ''
    elif isinstance(node, rdflib.Literal):
        return str(node.value).replace('"', r'\"')
    else:
        for prop, label in ontology.preferredLabel(node):
            return label
        s = node.n3(model.namespace_manager)
        if s.startswith('<'):
            return s[1:len(s) - 1]
        else:
            return prefixre.sub('', s)
            #return s

@lru_cache()
def nodeid(node):
    # TODO: avoid collisions
    return 'n{:016x}'.format(id(node))
    #return 'n' + str(node).translate(str(node).maketrans(dict.fromkeys(string.punctuation)))

@lru_cache()
def degree(node):
    if isinstance(node, rdflib.Literal):
        indegree = 1
    else:
        indegree = len(list(model.subjects(object = node)))
    outdegree = len(list(model.objects(subject = node)))
    return indegree + outdegree

dot = subprocess.Popen(['neato',
                        '-Tpdf',
                        '-Goutputorder=edgesfirst',
                        '-Goverlap=vpsc',
                        '-GK=100',
                        '-Nstyle=filled',
                        '-Nfillcolor=white',
                        '-Nfontname=Mono',
                        '-Npenwidth=0.5'],
                       stdin = subprocess.PIPE,
                       stdout = open('graph.pdf', 'w+'),
                       universal_newlines = True)

print('digraph graphname {', file = dot.stdin)
nodes = set()
for subj, pred, obj in model:
    for node in (subj, obj):
        if not isinstance(node, rdflib.Literal) and node not in nodes:
            print(nodeid(node), '[label="{}" shape={} fontsize="{}"]'.format(
                getlabel(node), 'point' if isinstance(node, rdflib.BNode) else 'ellipse',
                str(8*(1 + math.log(degree(node), 10)))), file = dot.stdin)
            nodes.add(node)

for subj, pred, obj in model:
    if isinstance(obj, rdflib.Literal):
        objid = nodeid(subj) + '_' + nodeid(pred) + '_' + nodeid(obj)
        print(objid, '[label="{}" shape=rectangle fontsize="{}"]'.format(
            getlabel(obj), str(8*(1 + math.log(degree(obj), 10)))), file = dot.stdin)
    else:
        objid = nodeid(obj)
    print('{} -> {} [label="{}" len={}]'.format(
        nodeid(subj), objid, getlabel(pred),
        str(0.5*(degree(subj)**2 + degree(obj)**2)**(1/2))), file = dot.stdin)
print('}', file = dot.stdin)

dot.stdin.close()
dot.wait()

print('Wrote graph.pdf', file = sys.stdout)
#os.startfile(graph.pdf)
